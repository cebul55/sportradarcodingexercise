package com.bcybulsk.scoreboard;

import com.bcybulsk.scoreboard.model.Game;

import java.util.List;

public interface ScoreBoard {
    public Game startGame(String homeTeamName, String awayTeamName);

    public void endGame(String homeTeamName, String awayTeamName);

    public Game updateGame(String homeTeamName, int homeTeamScore, String awayTeamName, int awayTeamScore);

    public String getGamesSummary();

    public List<Game> getGames();

    public int getNumberOfGames();

    public Game getGame(String homeTeamName, String awayTeamName);
}
