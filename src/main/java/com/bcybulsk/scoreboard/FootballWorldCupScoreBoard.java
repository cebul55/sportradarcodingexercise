package com.bcybulsk.scoreboard;

import com.bcybulsk.scoreboard.model.Game;
import com.bcybulsk.scoreboard.model.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FootballWorldCupScoreBoard implements ScoreBoard {

    private final ArrayList<Game> games;

    public FootballWorldCupScoreBoard() {
        games = new ArrayList<>();
    }

    @Override
    public Game startGame(String homeTeamName, String awayTeamName) {
        Game gameFound = getGame(homeTeamName, awayTeamName);
        if(gameFound != null)
            return gameFound;

        Game g = new Game(homeTeamName, awayTeamName);
        games.add(g);
        return g;
    }

    @Override
    public void endGame(String homeTeamName, String awayTeamName) {
        Game g = getGame(homeTeamName, awayTeamName);
        games.remove(g);
    }

    @Override
    public Game updateGame(String homeTeamName, int homeTeamScore, String awayTeamName, int awayTeamScore) {
        Game g = getGame(homeTeamName, awayTeamName);
        if(g == null)
            return null;

        g.updateScore(homeTeamScore, awayTeamScore);
        return g;
    }

    @Override
    public String getGamesSummary() {
        if(games.size() == 0)
            return "";

        ArrayList<Game> gamesCopy = new ArrayList<>(games);
        StringBuilder summaryString = new StringBuilder();
        Collections.reverse(gamesCopy);
        gamesCopy.sort(Comparator.comparing(Game::getTotalScore).reversed());
        int i = 1;
        for(Game g : gamesCopy) {
            summaryString
                    .append(i)
                    .append(". ")
                    .append(g.toString())
                    .append("\n");
            i++;
        }
        return summaryString.toString();
    }

    @Override
    public List<Game> getGames() {
        return games;
    }

    @Override
    public int getNumberOfGames() {
        return games.size();
    }

    @Override
    public Game getGame(String homeTeamName, String awayTeamName) {
        return games.stream()
                .filter(game -> (game.getHomeTeam().getName().equalsIgnoreCase(homeTeamName)
                        && game.getAwayTeam().getName().equalsIgnoreCase(awayTeamName)))
                .findAny()
                .orElse(null);
    }

}
