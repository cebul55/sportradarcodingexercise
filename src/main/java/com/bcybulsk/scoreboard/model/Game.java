package com.bcybulsk.scoreboard.model;

public class Game {
    private final static int MIN_SCORE = 0;
    private final Team homeTeam;
    private final Team awayTeam;
    private int homeTeamScore = MIN_SCORE;
    private int awayTeamScore = MIN_SCORE;

    public Game(Team homeTeam, Team awayTeam) {
        if(homeTeam == null || awayTeam == null)
            throw new IllegalArgumentException("Can not initialize game with null team.");
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
    }

    public Game(String homeTeamName, String awayTeamName) {
        this(new Team(homeTeamName), new Team(awayTeamName));
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public int getAwayTeamScore() {
        return awayTeamScore;
    }

    public void updateScore(int newHomeTeamScore, int newAwayTeamScore) {
        homeTeamScore = newHomeTeamScore >= MIN_SCORE ? newHomeTeamScore : MIN_SCORE;
        awayTeamScore = newAwayTeamScore >= MIN_SCORE ? newAwayTeamScore : MIN_SCORE;
    }

    public int getTotalScore() {
        return homeTeamScore + awayTeamScore;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(homeTeam.getName())
                .append(" ")
                .append(homeTeamScore)
                .append(" - ")
                .append(awayTeam.getName())
                .append(" ")
                .append(awayTeamScore);
        return stringBuilder.toString();
    }
}
