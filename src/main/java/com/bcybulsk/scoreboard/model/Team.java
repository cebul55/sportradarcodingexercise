package com.bcybulsk.scoreboard.model;

public class Team {
    private final String name;

    public Team(String name) {
        if(name == null)
            throw new IllegalArgumentException("Can not initialize team with null name.");
        if(name.equals(""))
            throw new IllegalArgumentException("Team name can not be empty.");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if(o == this)
            return true;
        if(!(o instanceof Team))
            return false;
        Team other = (Team) o;
        return getName().equalsIgnoreCase(other.getName());
    }
}
