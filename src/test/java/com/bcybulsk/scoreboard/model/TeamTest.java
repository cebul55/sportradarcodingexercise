package com.bcybulsk.scoreboard.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeamTest {

    @Test
    public void givenTeamWithName_whenGetIsCalled_thenNameStringIsEqual() {
        String teamName = "Brazil";
        Team brazilTeam = new Team(teamName);
        assertEquals("Brazil", brazilTeam.getName());
    }

    @Test
    public void given2Teams_whenNamesAreCompared_thenNamesAreNotEqual() {
        String teamNameBrazil = "Brazil";
        String teamNameArgentina = "Argentina";
        Team brazilTeam = new Team(teamNameBrazil);
        Team argentinaTeam = new Team(teamNameArgentina);
        assertNotEquals(brazilTeam.getName(), argentinaTeam.getName());
        assertEquals(teamNameBrazil, brazilTeam.getName());
        assertEquals(teamNameArgentina, argentinaTeam.getName());
    }

    @Test
    public void given2TeamsWithDifferentNames_whenCompared_teamsAreNotEqual() {
        String teamNameBrazil = "Brazil";
        String teamNameArgentina = "Argentina";
        Team brazilTeam = new Team(teamNameBrazil);
        Team argentinaTeam = new Team(teamNameArgentina);
        assertFalse(brazilTeam.equals(argentinaTeam));
    }

    @Test
    public void given2TeamsWithSameNames_whenCompared_teamsAreEqual() {
        String teamNameBrazil = "Brazil";
        Team team1 = new Team(teamNameBrazil);
        Team team2 = new Team(teamNameBrazil);
        assertEquals(team2, team1);
    }

    @Test
    public void givenTeamAndOtherObject_whenCompared_areNotEqual() {
        Team team = new Team("Brazil");
        String brazilString = "Brazil";
        assertFalse(team.equals(brazilString));
    }

    @Test
    public void givenNewTeamInitializedWithNullOrEmptyName_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Team("");
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new Team(null);
        });
    }
}