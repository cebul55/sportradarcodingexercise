package com.bcybulsk.scoreboard.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    public void givenNewGame_ScoreAndTeamsAreNotNull() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        assertNotNull(game.getHomeTeam());
        assertNotNull(game.getAwayTeam());
        assertNotNull(game.getHomeTeamScore());
        assertNotNull(game.getAwayTeamScore());
    }

    @Test
    public void givenNewGame_ScoresAreSetToZero() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        assertEquals(0, game.getHomeTeamScore());
        assertEquals(0, game.getAwayTeamScore());
    }

    @Test
    public void givenNewGame_addedUsingTeamNamesOnly_ScoreAndTeamsAreNotNull() {
        Game game = new Game("team1", "team2");
        assertNotNull(game.getHomeTeam());
        assertNotNull(game.getAwayTeam());
        assertNotNull(game.getHomeTeamScore());
        assertNotNull(game.getAwayTeamScore());
    }

    @Test
    public void givenNewGame_addedUsingTeamNamesOnly_ScoresAreSetToZero() {
        Game game = new Game("team1", "team2");
        assertEquals(0, game.getHomeTeamScore());
        assertEquals(0, game.getAwayTeamScore());
    }

    @Test
    public void givenNewGame_teamObjectIsReturned() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        assertTrue(game.getHomeTeam() instanceof Team);
        assertTrue(game.getAwayTeam() instanceof Team);
    }

    @Test
    public void givenNewGameInitializedWithSpecificTeams_correctTeamsAreReturned() {
        Team homeTeam = new Team("Brazil");
        Team awayTeam = new Team("Argentina");
        Game game = new Game(homeTeam, awayTeam);
        assertEquals(homeTeam, game.getHomeTeam());
        assertEquals(awayTeam, game.getAwayTeam());
    }

    @Test
    public void givenNewGameInitializedWithNullValues_throwsIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class, () -> {
            new Game("", null);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new Game(null, "");
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new Game(new Team("1"), null);
        });
        assertThrows(IllegalArgumentException.class, () -> {
            new Game(null, new Team("2"));
        });
    }

    @Test
    public void givenScoreUpdate_ScoreChanges() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        assertEquals(0, game.getHomeTeamScore());
        assertEquals(0, game.getAwayTeamScore());
        game.updateScore(1,2);
        assertEquals(1, game.getHomeTeamScore());
        assertEquals(2, game.getAwayTeamScore());
    }

    @Test
    public void givenScoreUpdateToNegativeValue_minScoreIsZero() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        game.updateScore(-1,-2);
        assertEquals(0, game.getHomeTeamScore());
        assertEquals(0, game.getAwayTeamScore());
    }

    @Test
    public void givenNewGame_totalScoreIsZero() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        assertEquals(0, game.getTotalScore());
    }

    @Test
    public void givenGameWithUpdatesScore_whenGetTotalIsCalled_SumIsCalculated() {
        Game game = new Game(new Team("team1"), new Team("team2"));
        int awayScore = 5;
        int homeScore = 3;
        game.updateScore(homeScore, awayScore);
        assertEquals(homeScore + awayScore, game.getTotalScore());
    }

    @Test
    public void givenNewGame_whenToStringIsCalled_initialScoreIsWritten() {
        String team1 = "homeTeam";
        String team2 = "awayTeam";
        Game game = new Game(new Team(team1), new Team(team2));
        String expectedString = team1 + " 0 - " + team2 + " 0";
        assertEquals(expectedString, game.toString());
    }

    @Test
    public void givenGame_whenToStringIsCalled_correctScoreIsWritten() {
        String team1 = "homeTeam";
        String team2 = "awayTeam";
        Game game = new Game(new Team(team1), new Team(team2));
        game.updateScore(1, 2);
        String expectedString = team1 + " 1 - " + team2 + " 2";
        assertEquals(expectedString, game.toString());
    }
}