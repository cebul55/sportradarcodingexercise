package com.bcybulsk.scoreboard;

import com.bcybulsk.scoreboard.model.Game;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FootballWorldCupScoreBoardTest {

    @Test
    public void givenNewScoreBoard_whenStartGameIsCalled_newGameIsCreated() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        Game g = scoreBoard.startGame("1", "2");
        assertNotNull(g);
    }

    @Test
    public void givenNewScoreBoard_whenStartGameIsCalled_newGameIsCreatedWithCorrecTeamNamesAndScore() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        Game g = scoreBoard.startGame(homeTeam, awayTeam);
        assertEquals(homeTeam, g.getHomeTeam().getName());
        assertEquals(awayTeam, g.getAwayTeam().getName());
        assertEquals(0, g.getHomeTeamScore());
        assertEquals(0, g.getAwayTeamScore());
    }

    @Test
    public void givenNewScoreBoardWithOneGame_getGames_listWithOneGameIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        Game createdGame = scoreBoard.startGame(homeTeam, awayTeam);
        List<Game> games = scoreBoard.getGames();
        assertEquals(1, games.size());
        assertSame(createdGame, games.get(0));
    }

    @Test
    public void givenNewScoreBoardWithOneGame_getGames_listWithOneGameIsReturnedAndSameNumberOfGames() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        Game createdGame = scoreBoard.startGame(homeTeam, awayTeam);
        List<Game> games = scoreBoard.getGames();
        assertEquals(1, games.size());
        assertEquals(1, scoreBoard.getNumberOfGames());
    }

    @Test
    public void givenNewScoreBoardWithOneGame_getGames_listWithOneGameAndCorrectNamesIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        List<Game> games = scoreBoard.getGames();
        assertEquals(1, games.size());
        Game g = games.get(0);
        assertEquals(homeTeam, g.getHomeTeam().getName());
        assertEquals(awayTeam, g.getAwayTeam().getName());
    }

    @Test
    public void givenScoreBoard_whenSecondGameWithSameTeamsIsAdded_gamesListDoesNotGrow() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        assertEquals(1, scoreBoard.getNumberOfGames());
        scoreBoard.startGame(homeTeam, awayTeam);
        assertEquals(1, scoreBoard.getNumberOfGames());
    }

    @Test
    public void givenScoreBoard_whenSecondGameWithSameTeamsIsAdded_gamesListDoesNotGrowForDifferentLetterCases() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        scoreBoard.startGame("homeTeam", "awayTeam");
        assertEquals(1, scoreBoard.getNumberOfGames());
        scoreBoard.startGame("HomeTeam", "AwayTeam");
        assertEquals(1, scoreBoard.getNumberOfGames());
        scoreBoard.startGame("hometeam", "awayteam");
        assertEquals(1, scoreBoard.getNumberOfGames());
        scoreBoard.startGame("hometeam", "AwayTeam");
        assertEquals(1, scoreBoard.getNumberOfGames());
        scoreBoard.startGame("HomeTeam", "awayteam");
        assertEquals(1, scoreBoard.getNumberOfGames());
    }

    @Test
    public void givenScoreBoard_whenGetGameIsCalledWithNonExistingValues_nullIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
        }
        Game g = scoreBoard.getGame("20", "30");
        assertNull(g);
    }

    @Test
    public void givenScoreBoard_whenGetGameIsCalled_existingGameIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
        }
        Game g = scoreBoard.getGame("2", "3");
        assertNotNull(g);
        assertEquals("2", g.getHomeTeam().getName());
        assertEquals("3", g.getAwayTeam().getName());
    }

    @Test
    public void givenScoreBoardWithSingleGame_whenGameEnds_gamesListIsEmpty() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        scoreBoard.endGame(homeTeam, awayTeam);
        assertEquals(0, scoreBoard.getNumberOfGames());
        assertEquals(0, scoreBoard.getGames().size());
    }

    @Test
    public void givenScoreBoardWithTwoGames_whenGameEnds_correctGameIsPreserved() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        scoreBoard.startGame("homeTeam1", "awayTeam2");
        scoreBoard.endGame(homeTeam, awayTeam);
        assertEquals(1, scoreBoard.getNumberOfGames());
        assertEquals(1, scoreBoard.getGames().size());
        assertNotNull(scoreBoard.getGame("homeTeam1", "awayTeam2"));
    }

    @Test
    public void givenScoreBoardWithGames_whenUpdateIsCalledOnNonExistingGame_nullIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        assertNull(scoreBoard.updateGame("1", 1, "2", 2));
    }

    @Test
    public void givenScoreBoardWithGames_whenUpdateIsCalledOnExistingGame_scoreIsUpdated() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
        }
        Game g = scoreBoard.updateGame("2", 2, "3", 3);
        assertNotNull(g);
        assertEquals(2, g.getHomeTeamScore());
        assertEquals(3, g.getAwayTeamScore());
    }

    @Test
    public void givenScoreBoardWithGames_whenUpdateIsCalledOnExistingGame_sameObjectIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
        }
        Game g = scoreBoard.updateGame("2", 2, "3", 3);
        assertSame(g, scoreBoard.getGame("2", "3"));
    }

    @Test
    public void givenEmptyBoard_whenSummaryIsCalled_EmptyStringIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        assertEquals("", summary);
    }

    @Test
    public void givenBoardWithSingleGame_whenSummaryIsCalled_singleGameStringIsReturned() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        assertEquals("1. homeTeam 0 - awayTeam 0\n", summary);
    }

    @Test
    public void givenBoardWithTwoGames_whenSummaryIsCalled_gameWithBiggerTotalIsReturnedFirst() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        String homeTeam = "homeTeam";
        String awayTeam = "awayTeam";
        scoreBoard.startGame(homeTeam, awayTeam);
        scoreBoard.updateGame(homeTeam, 3, awayTeam, 4);
        scoreBoard.startGame("team1", "team2");
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        assertEquals("1. homeTeam 3 - awayTeam 4\n2. team1 0 - team2 0\n", summary);
    }

    @Test
    public void givenBoardWithGames_whenSummaryIsCalled_summaryIsOrderedByTotalScores() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
            scoreBoard.updateGame(String.valueOf(i), i, String.valueOf(i+1), i+1);
        }
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        assertEquals("1. 8 8 - 9 9\n" +
                "2. 6 6 - 7 7\n" +
                "3. 4 4 - 5 5\n" +
                "4. 2 2 - 3 3\n" +
                "5. 0 0 - 1 1\n", summary);
    }

    @Test
    public void givenBoardWithGames_whenSummaryIsCalled_summaryScoresAreTheSameAndIsOrderedByAddedTimeDescending() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
            scoreBoard.updateGame(String.valueOf(i), 3, String.valueOf(i+1), 3);
        }
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        String expected = "1. 8 3 - 9 3\n" +
                "2. 6 3 - 7 3\n" +
                "3. 4 3 - 5 3\n" +
                "4. 2 3 - 3 3\n" +
                "5. 0 3 - 1 3\n";
        assertEquals(expected, summary);
    }

    @Test
    public void givenBoardWithDeletedGames_whenSummaryIsCalled_summaryScoresAreTheSameAndIsOrderedByAddedTimeDescending() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
            scoreBoard.updateGame(String.valueOf(i), 3, String.valueOf(i+1), 3);
        }
        scoreBoard.endGame("6", "7");
        scoreBoard.endGame("2", "3");
        String summary = scoreBoard.getGamesSummary();
        assertNotNull(summary);
        String expected = "1. 8 3 - 9 3\n" +
                "2. 4 3 - 5 3\n" +
                "3. 0 3 - 1 3\n";
        assertEquals(expected, summary);
    }

    @Test
    public void givenBoardWithGames_whenSummaryIsCalled_summaryScoresAreTheSameAndIsOrderedByAddedTimeDescending_andIsCorrectMultipleTimes() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        for(int i = 0; i < 10; i+=2) {
            scoreBoard.startGame(String.valueOf(i), String.valueOf(i+1));
            scoreBoard.updateGame(String.valueOf(i), 3, String.valueOf(i+1), 3);
        }
        String expected = "1. 8 3 - 9 3\n" +
                "2. 6 3 - 7 3\n" +
                "3. 4 3 - 5 3\n" +
                "4. 2 3 - 3 3\n" +
                "5. 0 3 - 1 3\n";
        assertEquals(expected, scoreBoard.getGamesSummary());
        assertEquals(expected, scoreBoard.getGamesSummary());
        assertEquals(expected, scoreBoard.getGamesSummary());
    }

    @Test
    public void testExample() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        scoreBoard.startGame("Mexico", "Canada");
        scoreBoard.startGame("Spain", "Brazil");
        scoreBoard.startGame("Germany", "France");
        scoreBoard.startGame("Uruguay", "Italy");
        scoreBoard.startGame("Argentina", "Australia");
        scoreBoard.updateGame("Uruguay", 6, "Italy", 6);
        scoreBoard.updateGame("Spain", 10, "Brazil", 2);
        scoreBoard.updateGame("Mexico", 0, "Canada", 5);
        scoreBoard.updateGame("Argentina", 3, "Australia", 1);
        scoreBoard.updateGame("Germany", 2, "France", 2);
        String expected = "1. Uruguay 6 - Italy 6\n" +
                "2. Spain 10 - Brazil 2\n" +
                "3. Mexico 0 - Canada 5\n" +
                "4. Argentina 3 - Australia 1\n" +
                "5. Germany 2 - France 2\n";
        assertEquals(expected, scoreBoard.getGamesSummary());
        System.out.println(scoreBoard.getGamesSummary());
    }

    @Test
    public void testExample_WhenCopyOfExistingGameIsAdded_SummaryDoesNotChange() {
        ScoreBoard scoreBoard = new FootballWorldCupScoreBoard();
        scoreBoard.startGame("Mexico", "Canada");
        scoreBoard.startGame("Spain", "Brazil");
        scoreBoard.startGame("Germany", "France");
        scoreBoard.startGame("Uruguay", "Italy");
        scoreBoard.startGame("Argentina", "Australia");
        scoreBoard.updateGame("Uruguay", 6, "Italy", 6);
        scoreBoard.updateGame("Spain", 10, "Brazil", 2);
        scoreBoard.updateGame("Mexico", 0, "Canada", 5);
        scoreBoard.updateGame("Argentina", 3, "Australia", 1);
        scoreBoard.updateGame("Germany", 2, "France", 2);
        String expected = "1. Uruguay 6 - Italy 6\n" +
                "2. Spain 10 - Brazil 2\n" +
                "3. Mexico 0 - Canada 5\n" +
                "4. Argentina 3 - Australia 1\n" +
                "5. Germany 2 - France 2\n";
        assertEquals(expected, scoreBoard.getGamesSummary());
        scoreBoard.startGame("Mexico", "Canada");
        assertEquals(expected, scoreBoard.getGamesSummary());
    }
}