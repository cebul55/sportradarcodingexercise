# Football World Cup Score Board

Coding Exercise written by Bartosz Cybulski

### Classes description: 

- Team class - represents team. Has only 1 immutable parameter team name

- Game class - represents single game. Has four parameters - homeTeam, awayTeam, homeTeamScore and awayTeamScore
. Scores default and minimum value is 0.

- ScoreBoard - interface

- FootballWorldCupScoreBoard - implements ScoreBoard interface. Represents Football World Cup Score Board. Has private field games, which
 is of class ArrayList -> Where
 first item is oldest, and last item is the newest game added to board. Adding duplicate games is prevented in
  startGame() method, by checking if game already exists.